# load_model_sample.py
from keras.models import load_model
from keras.preprocessing import image
import matplotlib.pyplot as plt
import numpy as np
import os
import tensorflow as tf


global model
global classes

classes = ["Chou-Fleur","Radis"]

def load_image(img_path, show=False):

    img = image.load_img(img_path, target_size=(256, 256))
    img_tensor = image.img_to_array(img)                    # (height, width, channels)
    return img_tensor

global model

# load model
model = load_model("model/model")

def prediction_path(path):
    
    # load a single image
    array = load_image(path)
    new_array = np.expand_dims(array, axis=0)
    new_array /= 255.
    # check prediction
    pred = np.argmax(model.predict(new_array))
    print(model.predict(new_array))
    print(classes[pred])

def prediction_array(array):
    
    # load a single image
    new_array = np.expand_dims(array, axis=0)
    new_array /= 255.
    # check prediction
    pred = np.argmax(model.predict(new_array))
    print(model.predict(new_array))
    print(classes[pred])
